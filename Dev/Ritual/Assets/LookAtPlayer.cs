﻿using UnityEngine;
using System.Collections;

public class LookAtPlayer : MonoBehaviour {
	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 position = transform.position;
		position.y = player.transform.position.y;
		transform.position = position;

		Vector3 direction = new Vector3 ();
		direction = (player.transform.position - transform.position).normalized;
		transform.up = direction;

	}
}
