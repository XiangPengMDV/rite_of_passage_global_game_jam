﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	float x,y;
	bool anim=false;
	// Use this for initialization
	void Start () {
	
		x=0.2f;
		y=transform.position.y;
	}

	public void OpenDoor()
	{
		anim=true;

			transform.position = new Vector3(transform.position.x-x,transform.position.y-0.05f,transform.position.z);
			x*=-1;
		if(transform.position.y <-y)
			Destroy (gameObject);

	}

	// Update is called once per frame
	void Update () {
	
		if(anim)OpenDoor();
	}
}
