﻿#pragma strict


var mins : float = 11.00;
var secs : float = 0.00;
var chalk : int = 10;

var timeString : String = "";
var chalkString : String = "";


function Start () {

	
}

function Update () {

	
	// reduce by 1 second
	secs -= Time.deltaTime;

	if (secs <= 0) {
		if (mins <= 0) {
			secs = 59.0;
			mins = 0.0;
		}
		secs = 59.0;
		mins -= 1.0;
	} else {
		
	}

	timeString = timeFormat(mins, secs);

	chalkString = chalkFormat(chalk);

	if (mins <= 0) {
		if (secs <= 0) {
			Application.LoadLevel("endScene");
		}
	}



} // end Update scope

function OnGUI () {

	// show on UI
	GUI.Box(new Rect(10, 10, 150, 20), timeString);
	GUI.Box(new Rect(10, 40, 150, 20), chalkString);

};

// sign time string
private function timeFormat (mins : float, secs : float) : String {

	// less than 1 min
	if (mins <= 0) {

		timeString = "Time Left: " + secs.ToString("0.0");
		// time out
		if (secs <= 0) {

			timeString = "Time Out!";

		}
	}
	 
	// more than 1 min  (mins > 0)
	else {

		timeString = "Time Left: " + mins.ToString("0") + " : " + secs.ToString("0");

	}

	return timeString;
};



private function chalkFormat (chalk : int) : String {
	chalkString = "Chalk Left: " + chalk.ToString("0");
	return chalkString;
};



