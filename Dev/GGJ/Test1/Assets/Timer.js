﻿#pragma strict


var mins : float = 11.00;
var secs : float = 0.00;
var chalk : int = 10;

var timeString : String = "";
var chalkString : String = "";

function Start () {

	
}

function Update () {

	
	// reduce by 1 second
	secs -= Time.deltaTime;

	secs = timeSecCheck(mins, secs);
	mins = timeMinCheck(mins, secs);

	timeString = timeFormat(mins, secs);

	chalkString = chalkFormat(chalk);



} // end Update scope

function OnGUI () {

	// show on UI
	GUI.Box(new Rect(10, 10, 150, 20), timeString);
	GUI.Box(new Rect(10, 40, 150, 20), chalkString);

};

// sign time string
private function timeFormat (mins : float, secs : float) : String {

	if (mins <= 0) {

		timeString = "Time Left: " + secs.ToString("0.0");

		if (secs <= 0) {

			timeString = "Time Out!";

		}

	} else {
	
	timeString = "Time Left: " + mins.ToString("0") + ":" + secs.ToString("0");

	}

	return timeString;
};


private function timeMinCheck (mins : float, secs : float) : float {

	// less than 1 min
	if (mins <=0) {
		mins = 0;
		if (secs <= 0) {
			mins = 0;
			secs = 0;
			return mins;
		}
		return mins;
	}
	// more than 1 min
	else {
		if (secs <= 0) {
			mins -= 1;
			secs = 59;
			return mins;
		}
		return mins;
	};

}; // min check

private function timeSecCheck (mins : float, secs : float) : float {

	// less than 1 min
	if (mins <=0) {
		mins = 0;
		if (secs <= 0) {
			mins = 0;
			secs = 0;
			return secs;
		}
		return secs;
	}
	// more than 1 min
	else {
		if (secs <= 0) {
			mins -= 1;
			secs = 59;
			return secs;
		}
		return secs;
	};

}; // sec check

private function chalkFormat (chalk : int) : String {
	chalkString = "Chalk Left: " + chalk.ToString("0");
	return chalkString;
};



