﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    Transform target;
    public float minSpeed = 25.0f;
    public float maxSpeed = 40.0f;
    float speed;


    float fixedY = 10.0f;

    public int chalk = 10;
    public int[] book = new int[4];
    int books = 0;

    public Camera gameCamera;

    public GameObject[] allMarks;

    List<GameObject> availableMarks;
    int curMarkIndex = 0;
    GameObject curMark;

    bool rightClickDown = false;
    float walkLerp = 0.0f;
    bool walkLerpDown = true;

    bool jumping = false;
    float jumpSpeed = 25.0f;
    float gravity = 30.0f;

    // Use this for initialization
    void Start()
    {
        speed = minSpeed;
        availableMarks = new List<GameObject>();
        availableMarks.Add(allMarks[0]);
        curMark = availableMarks[0];
        curMarkIndex = 0;

        foreach (GameObject mark in allMarks)
        {
            mark.GetComponentInChildren<RitualSymbol>().MakeTransparent();
            mark.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        MovementInput();
        RayInput();
        CheckRayEnemy();
    }

    void OnCollisionEnter(Collision C)
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        if (C.gameObject.tag == "Chalk")
        {
            chalk += 10;
            //GameObject.Find("Prompt").GetComponent<Prompt>().Prompter(C.gameObject.tag);
            Destroy(C.gameObject);
            Debug.Log("Collision with chalk");
        }
        else if (C.gameObject.tag == "Book")
        {
            int bookId = C.gameObject.GetComponent<Book>().bookId;
            book[books++] = bookId;
            availableMarks.Add(allMarks[bookId]);
            //GameObject.Find("Prompt").GetComponent<Prompt>().Prompter(C.gameObject.tag);
            Destroy(C.gameObject);
            Debug.Log("Collision with book");
        }
    }

    void MovementInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = maxSpeed;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = minSpeed;
        }

        Vector3 position = new Vector3();
        position = transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            position += transform.forward * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            position += -transform.forward * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            position += -transform.right * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            position += transform.right * speed * Time.deltaTime;
        }
        
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(!jumping)
                jumping = true;
        }


        if (!jumping)
        {
            if (walkLerpDown)
            {
                walkLerp -= Time.deltaTime * (speed / 5);
                if (walkLerp < -0.5f)
                    walkLerpDown = false;
            }
            else
            {
                walkLerp += Time.deltaTime * (speed / 5);
                if (walkLerp > 0.5f)
                    walkLerpDown = true;
            }

            if (transform.position != position)
                position.y = fixedY + walkLerp;
            else
                position.y = fixedY;
        }
        else
        {
            position.y += jumpSpeed * Time.deltaTime;

            if (position.y <= fixedY)
            {
                jumpSpeed = 25.0f;
                position.y = fixedY;
                jumping = false;
                walkLerp = 0.0f;
                walkLerpDown = true;
            }
            else {
                jumpSpeed -= gravity * Time.deltaTime;
            }
        }

        transform.position = position;
    }
    

    public GameObject enemyToDestroy = null;
    bool attackNextTimeSeen = false;
    int attackCounter = 0;

    void CheckRayEnemy()
    {
        if(enemyToDestroy && attackNextTimeSeen == false)
        {
            Vector3 enemyScreenPos = gameCamera.WorldToScreenPoint(enemyToDestroy.transform.position);
            if (enemyScreenPos.x < 0 || enemyScreenPos.x > Screen.width)
            {
                Vector3 toEnemy = enemyToDestroy.transform.position - transform.position;
                toEnemy.Normalize();
                Vector3 position = transform.position - toEnemy * 20.0f;
                position.y = 0;
                enemyToDestroy.transform.position = position;
                attackNextTimeSeen = true;
                attackCounter++;
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Ray vRay = gameCamera.ScreenPointToRay(new Vector3(i * (Screen.width / 10), j * (Screen.height / 10), 0));
                    RaycastHit hit;
                    if (Physics.Raycast(vRay.origin, vRay.direction, out hit))
                    {
                        if (hit.collider.gameObject.tag == "Enemy")
                        {
                            if(hit.collider.gameObject != enemyToDestroy)
                            {
                                enemyToDestroy = hit.collider.gameObject;
                                attackNextTimeSeen = false;
                                break;
                            }

                            if(enemyToDestroy == hit.collider.gameObject && attackNextTimeSeen)
                            {
                                enemyToDestroy = hit.collider.gameObject;
                                attackNextTimeSeen = false;

                                if (attackCounter == 3)
                                {
                                    enemyToDestroy.GetComponent<Enemy>().SetStanding(false);
                                    attackCounter = 0;
                                }
                            }
                            else
                            {
                                enemyToDestroy = hit.collider.gameObject;
                            }
                        }
                    }
                }
            }
        }
    }

    void RayInput()
    {
        if (Input.GetMouseButtonDown(1))
        {
            rightClickDown = true;
            curMark.SetActive(true);
        }
        if (Input.GetMouseButtonUp(1))
        {
            rightClickDown = false;
            curMark.SetActive(false);
        }

        if (rightClickDown)
        {
            RitualSymbol symbol = curMark.GetComponentInChildren<RitualSymbol>();
            Ray vRay = gameCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(vRay.origin, vRay.direction, out hit))
            {
                curMark.SetActive(true);
                curMark.transform.position = hit.point + (hit.normal * 0.1f);
                curMark.transform.LookAt(hit.point + hit.normal * 2);

                if ((symbol.tagToInteract == hit.collider.gameObject.tag || symbol.tagToInteract == "") )
                {
                    curMark.GetComponentInChildren<RitualSymbol>().MakeOpaque();

                    if (Input.GetMouseButtonDown(0))
                    {
                        if (chalk > 0)
                        {
                            GameObject newMark = Instantiate(curMark);
                            newMark.GetComponentInChildren<RitualSymbol>().MakeOpaque();
                            newMark.transform.position = curMark.transform.position;
                            newMark.transform.rotation = curMark.transform.rotation;
                            newMark.transform.localScale = transform.localScale;
                            chalk--;

                            newMark.transform.parent = hit.collider.transform;
                            symbol.Activate(hit.collider.gameObject);
                        }
                    }
                }
                else
                {
                    symbol.MakeTransparent();
                }
            }
            else
            {
                symbol.MakeTransparent();
            }
        }


        if (rightClickDown)
        {
            float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
            if (mouseWheel < 0.0f)
            {
                curMarkIndex++;
                if (curMarkIndex >= availableMarks.Count)
                    curMarkIndex = 0;
            }
            else if (mouseWheel > 0.0f)
            {
                curMarkIndex--;
                if (curMarkIndex < 0)
                    curMarkIndex = availableMarks.Count - 1;
            }
            curMark.SetActive(false);
            curMark = availableMarks[curMarkIndex];
            curMark.SetActive(true);
        }
    }

    bool IsMarkAvailable(int id)
    {
        foreach (int b in book)
        {
            if (b == id) return true;
        }
        return false;
    }

}

