﻿using UnityEngine;
using System.Collections;

public class RitualSymbol : MonoBehaviour {
    float opacity = 0.35f;
    public string tagToInteract = "Door";

    // Use this for initialization
    void Start()
    {
    }
	
	// Update is called once per frame
	public void MyUpdate () {

    }


    // Makes the ritual symbol opaque
    public void MakeOpaque()
    {
        Renderer renderer = GetComponent<Renderer>();
        Color color = renderer.material.color;
        color.a = 1.0f;
        renderer.material.color = color;
    }

    // Sets the ritual symbol's transparency to the variable "opacity"
    public void MakeTransparent()
    {
        Debug.Log("Making transparent");
        Renderer renderer = GetComponent<Renderer>();
        Color color = renderer.material.color;
        color.a = opacity;
        renderer.material.color = color;
    }

    public void Activate(GameObject objectCollided)
    {
        if(tagToInteract == "Door")
        {
            objectCollided.GetComponent<Door>().OpenDoor();
        }
    }

}
