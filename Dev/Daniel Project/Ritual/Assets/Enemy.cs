﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    bool visible;
    public GameObject player;

    public GameObject dirLight;

    bool standing = true;
    public AudioClip attackSound;


    // Use this for initialization
    void Start () {

        Renderer renderer = GetComponent<Renderer>();
        visible = renderer.isVisible;
    }

    // Update is called once per frame
    void Update () {
        Vector3 position = player.transform.position;
        position.y = 0;

        Vector3 position2 = transform.position;
        position2.y = 0;

        Vector3 forward = position - position2;
        forward.Normalize();
        transform.rotation = Quaternion.LookRotation(forward, new Vector3(0, 1, 0));

        if(!standing)
        {
            dirLight.SetActive(true);
            transform.position += forward * 60.0f * Time.deltaTime;
        }
    }


    public bool IsStanding() { return standing; }
    public void SetStanding(bool st) {
        standing = st;
        if(!standing)
        {
            GetComponent<AudioSource>().clip = attackSound;
            GetComponent<AudioSource>().Play();
        }
    }

    void OnCollisionEnter(Collision C)
    {
        if(C.gameObject.tag == "Player")
        {
            dirLight.SetActive(false);
            Destroy(gameObject);
            player.GetComponent<Player>().enemyToDestroy = null;
        }
    }
}
