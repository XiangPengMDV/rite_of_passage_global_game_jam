﻿using UnityEngine;
using System.Collections;

public class TextTimer : MonoBehaviour {

	
	public float mins,umins;
	public float secs,usecs ;
	int chalk=10;
	Vector2 timePass;


	string timeString= "";
	string chalkString = "";

	void Start()
	{
		umins=0f;
		usecs=0f;
		mins = 15.00f;
		secs = 0.0f;
	}

	void Update () {

        if(mins < 2)
        {
            if(!GetComponent<AudioSource>().isPlaying)
                GetComponent<AudioSource>().Play();
        }

	//	print (mins + ":" + secs);

		chalk = GameObject.Find("Player").GetComponent<Player>().chalk;
		// reduce by 1 second

		secs -= Time.deltaTime;
				
		if(mins==0 && secs<0)
		{GameOver();}

		if(secs<=0)
		{
			mins-=1;
			secs=59;
		}
		if(mins<=0) mins=0;

		timePass = new Vector2 (mins,secs);
		//chalk = GameObject.Find("Player").GetComponentsInChildren.<"Player">().chalk;
		
		timeString = timeFormat(mins, secs);
		
		chalkString = chalkFormat(chalk);
        
	} // end Update scope
	void OnGUI () {
		
		// show on UI
		GUI.Box(new Rect(10, 10, 150, 20), timeString);
		GUI.Box(new Rect(10, 40, 150, 20), chalkString);
		
	}

	public void SetTime(Vector2 update)
	{
		umins= update.x;
		usecs = update.y;

	}
	
	// sign time string
	string timeFormat (float mins , float secs) {

		if(umins!=0 && usecs!=0)
		{
			mins=umins;
			secs=usecs;
			timeString = "Time Left: " + umins.ToString("0") + ":" + usecs.ToString("0");
			umins=0;
			usecs=0;

		}

		if (mins == 0) {
			
			timeString = "Time Left: " + secs.ToString("0.0");
			
			if (secs <= 0) {
				
				timeString = "Time Out!";
				
			}
			
		} else {
			
			timeString = "Time Left: " + mins.ToString("0") + ":" + secs.ToString("0");

		}
		
		return timeString;
	}
	

		
	//} // sec check
	
	string chalkFormat (int chalk)  {
		chalkString = "Chalk Left: " + chalk.ToString("0");
		return chalkString;
	}

	void GameOver()
	{
		//Time.timeScale=0;
		GUIText message = GetComponent<GUIText>();
		message.enabled=true;
		message.transform.position= new Vector3(0.5f,0.5f,0f);
		message.text = "Game Over";
		Time.timeScale=0;
        

	}

	public Vector2 CurrentTime ()
	{
		return(timePass);
	}

	
	
}
