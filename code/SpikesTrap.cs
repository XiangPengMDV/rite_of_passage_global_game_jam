﻿using UnityEngine;
using System.Collections;

public class SpikesTrap : MonoBehaviour {


    public float yOffset = 10.0f;
    public float upSpeed = 10.0f;
    public float downSpeed = 3.0f;

    public float startDelay = 0.0f;
    public float attackDelay = 3.0f;
    public float lowerDelay = 1.0f;

    float curOffset = 0;
    float curTimer;
    float originalYPos;

    public AudioClip trapUpSound;
    public AudioClip trapDownSound;
    
    enum State { WAITING, GOING_UP, UP_WAIT, GOING_DOWN, DOWN_WAIT };
    State state;

	// Use this for initialization
	void Start () {
        state = State.WAITING;
        curTimer = 0.0f;
        originalYPos = transform.position.y;
    }

    // Update is called once per frame
    void Update () {
	    switch(state)
        {
            case State.WAITING:
                curTimer += Time.deltaTime;
                if(curTimer > startDelay)
                {
                    state = State.GOING_UP;
                    GetComponent<AudioSource>().clip = trapUpSound;
                    GetComponent<AudioSource>().Play();

                    curTimer = 0.0f;
                }
                break;
            case State.GOING_UP:
                curOffset += upSpeed * Time.deltaTime;
                if(curOffset > yOffset)
                {
                    state = State.UP_WAIT;
                    curTimer = 0.0f;
                }
                break;
            case State.UP_WAIT:
                curTimer += Time.deltaTime;
                if(curTimer > lowerDelay)
                {
                    state = State.GOING_DOWN;
                    GetComponent<AudioSource>().clip = trapDownSound;
                    GetComponent<AudioSource>().Play();
                    curTimer = 0.0f;
                }
                break;
            case State.GOING_DOWN:
                curOffset -= downSpeed * Time.deltaTime;
                if(curOffset < 0)
                {
                    curOffset = 0.0f;
                    state = State.DOWN_WAIT;
                }
                break;
            case State.DOWN_WAIT:
                curTimer += Time.deltaTime;
                if(curTimer > attackDelay)
                {
                    state = State.GOING_UP;
                    GetComponent<AudioSource>().clip = trapUpSound;
                    GetComponent<AudioSource>().Play();
                    curTimer = 0.0f;
                }
            break;
        }

        Vector3 position = transform.position;
        position.y = originalYPos + curOffset;
        transform.position = position;
	}
}
