﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Checkpoint : MonoBehaviour {


	bool first = true;
	float dtime = 0f;
	List<GameObject> bookChk;
	GameObject temp;
	Vector2 currentTime;

	// Use this for initialization
	void Start () {

		//bookChk = new int[6];

	}
	
	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Player")
		{

			GameObject.Find("Spawner").transform.position = new Vector3(transform.position.x,14,transform.position.z);
			if(first)
			{
				dtime = Time.timeSinceLevelLoad+2;
	
				currentTime = GameObject.Find("UI").GetComponent<TextTimer>().CurrentTime();
				//print (currentTime.x + ":" + currentTime.y);
				bookChk = GameObject.Find("Player").GetComponent<Player>().availableMarks;
				GameObject.Find("Spawner").GetComponent<Respawn>().bookChk = bookChk;
				GameObject.Find("Spawner").GetComponent<Respawn>().countBooks = bookChk.Count;
				GameObject.Find("Spawner").GetComponent<Respawn>().checkMin = currentTime.x;
				GameObject.Find("Spawner").GetComponent<Respawn>().checkSec = currentTime.y;
			}


		}
	}

	void ChkPrompt()
	{
		GUIText g = GameObject.Find ("UI").GetComponent<GUIText>();
        Debug.Log(g);

        if (dtime>Time.timeSinceLevelLoad && first)
		{
            g.enabled = true;
            g.text = "Checkpoint";
			g.transform.position = new Vector3(0.5f,0.5f,0f);
		}
		else 
		{
			g.enabled = false;
			dtime=0;
			first = false;
		}
	}

	// Update is called once per frame
	void Update () {
		if(dtime!=0)
		ChkPrompt();
	}
}
