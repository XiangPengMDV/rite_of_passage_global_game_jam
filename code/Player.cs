﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Player : MonoBehaviour
{
    [System.Serializable]
    public class SoundRandomizer
    {
        public string groundTag;
        public AudioClip[] sounds;
    }

    // Speed the player walks
    public float minSpeed = 25.0f;

    // Speed the player sprints
    public float maxSpeed = 40.0f;

    // Amount of chalk the player starts with
    public int chalk = 10;

    // Array of books acquired by the player
    int[] book = new int[4];

    // Camera of the game
    public Camera gameCamera;

    // All marks the player can possibly acquire
    public GameObject[] allMarks;

    // Book being held
    public GameObject holdingBook;
    public GameObject holdingChalk;

    // Audio that plays when the player walks
    public SoundRandomizer[] stepAudio;

    // Audio that plays when the player jumps
    public SoundRandomizer[] jumpStartAudio;

    // Audio that plays when the player hits the ground after a jump
    public SoundRandomizer[] jumpLandAudio;

    // Current speed of the player
    float speed;

    // Monster encountered by the player (Scare)
    GameObject enemyMonster = null;

    // If this is true, the monster will attack the player the next time he's seen
    bool attackNextTimeSeen = false;

    // Counter of times the player has seen the monster
    int attackCounter = 0;

    // Amount of books acquired by the player
    int bookCount = 0;

    // List of marks available to be placed by the player
    public List<GameObject> availableMarks;

    // Index of the currently selected mark to be placed into the game's world
    public int curMarkIndex = 0;

    // Reference to the current mark the player is about to place
    GameObject curMark;

    // True when the player has the right button of the mouse clicked
    bool rightClickDown = false;

    // True when the player is jumping
    bool jumping = false;

    // Speed the player jumps on
    float jumpSpeed = 25.0f;

    // "Gravity", used to make the player fall after the jump
    float gravity = 40.0f;

    // Cur terrain the player is colliding with (ground type) - used to play sounds
    string curTerrain = "Concrete";

    // We're gonna keep track of two audio sources
    AudioSource audioStep;
    AudioSource audioExtra;

    public AudioClip disableTrapAudio;

    public AudioClip drawChalkAudio;

    // Use this for initialization
    void Start()
    {
        speed = minSpeed;
        availableMarks = new List<GameObject>();

        // X - Available from the beginning
        availableMarks.Add(allMarks[0]);
        // V - Available from the beginning
        availableMarks.Add(allMarks[5]);

        curMark = availableMarks[0];
        curMarkIndex = 0;

        foreach (GameObject mark in allMarks)
        {
            mark.GetComponentInChildren<RitualSymbol>().MakeTransparent();
            mark.SetActive(false);
        }

        AudioSource[] sources = GetComponents<AudioSource>();
        audioStep = sources[0];
        audioExtra = sources[1];

        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        MovementInput();
        RayInput();
        CheckRayEnemy();
    }

    void OnCollisionEnter(Collision C)
    {
        if(C.collider.gameObject == enemyMonster)
        {
                Vector3 toEnemy = enemyMonster.transform.position - transform.position;
                toEnemy.Normalize();
                Vector3 position = transform.position - toEnemy * C.collider.bounds.extents.x * 3;
                position.y = 0;
                enemyMonster.transform.position = position;
                attackNextTimeSeen = true;
                attackCounter++;
        }

        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        // Collided with chalk - collect chalk
        if (C.gameObject.tag == "Chalk")
        {
            chalk += 10;

            // We're gonna play the collecting chalk sound
            audioExtra.clip = C.gameObject.GetComponent<AudioSource>().clip;
            audioExtra.Play();

            // Destroy the chalk pickup
            Destroy(C.gameObject);
            //GameObject.Find("Prompt").GetComponent<Prompt>().Prompter(C.gameObject.tag);
        }
        // Collided with book - collect book
        else if (C.gameObject.tag == "Book")
        {
            Book _book = C.gameObject.GetComponent<Book>();
            int bookId = _book.bookId;
            book[bookCount++] = bookId;

            // Add the mark of the book found to our array of marks available to use
            availableMarks.Add(allMarks[bookId]);

            // We're gonna play the collecting book sound
            audioExtra.clip = _book.gameObject.GetComponent<AudioSource>().clip;
            audioExtra.Play();

            // Destroy the book
            Destroy(C.gameObject);
            //GameObject.Find("Prompt").GetComponent<Prompt>().Prompter(C.gameObject.tag);
        }

        else if (C.gameObject.tag == "Concrete" || C.gameObject.tag == "Dirt" || C.gameObject.tag == "Marble")
        {
            curTerrain = C.gameObject.tag;
            jumpSpeed = 25.0f;
            jumping = false;
            gameCamera.GetComponent<CameraBehavior>().ResetLerp();
            PlayJumpLandSound();
        }
    }

    float positionBeforeJump;
    
    void MovementInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            speed = maxSpeed;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = minSpeed;
        }

        Vector3 position = new Vector3();
        position = transform.position;

        if (Input.GetKey(KeyCode.W))
        {
            position += transform.forward * speed * Time.deltaTime;
            gameCamera.GetComponent<CameraBehavior>().StartWalking();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            position += -transform.forward * speed * Time.deltaTime;
            gameCamera.GetComponent<CameraBehavior>().StartWalking();
        }
        else if (Input.GetKey(KeyCode.A))
        {
            position += -transform.right * speed * Time.deltaTime;
            gameCamera.GetComponent<CameraBehavior>().StartWalking();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            position += transform.right * speed * Time.deltaTime;
            gameCamera.GetComponent<CameraBehavior>().StartWalking();
        }
        else
        {
            gameCamera.GetComponent<CameraBehavior>().StopWalking();
        }
        if (jumping)
        {
            position.y += jumpSpeed * Time.deltaTime;
            jumpSpeed -= gravity * Time.deltaTime;
            if (position.y < positionBeforeJump)
            {
                position.y = positionBeforeJump;
                jumping = false;
                jumpSpeed = 25.0f;
                gameCamera.GetComponent<CameraBehavior>().ResetLerp();
                PlayJumpLandSound();
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!jumping)
            {
                positionBeforeJump = transform.position.y;
                jumping = true;
                PlayJumpStartSound();
            }
        }


        transform.position = position;
    }

    public void SetEnemy(GameObject en)
    {
        enemyMonster = en;
    }

    void CheckRayEnemy()
    {
        if (enemyMonster && attackNextTimeSeen == false)
        {
            Vector3 enemyScreenPos = gameCamera.WorldToScreenPoint(enemyMonster.transform.position);
            if (enemyScreenPos.x < 0 || enemyScreenPos.x > Screen.width)
            {
                Vector3 toEnemy = enemyMonster.transform.position - transform.position;
                toEnemy.Normalize();
                Vector3 position = transform.position - toEnemy *  20.0f;
                position.y = 0;
                enemyMonster.transform.position = position;
                attackNextTimeSeen = true;
                attackCounter++;
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Ray vRay = gameCamera.ScreenPointToRay(new Vector3(i * (Screen.width / 10), j * (Screen.height / 10), 0));
                    RaycastHit hit;
                    if (Physics.Raycast(vRay.origin, vRay.direction, out hit))
                    {
                        if (hit.collider.gameObject.tag == "Enemy")
                        {
                            if (hit.collider.gameObject != enemyMonster)
                            {
                                enemyMonster = hit.collider.gameObject;
                                attackNextTimeSeen = false;
                                enemyMonster.GetComponent<Enemy>().PlayBumpSound(0);
                                break;
                            }

                            if (enemyMonster == hit.collider.gameObject && attackNextTimeSeen)
                            {
                                enemyMonster = hit.collider.gameObject;
                                attackNextTimeSeen = false;

                                if (attackCounter == 3)
                                {
                                    enemyMonster.GetComponent<Enemy>().SetStanding(false);
                                    attackCounter = 0;
                                }
                            }
                            else
                            {
                                enemyMonster = hit.collider.gameObject;
                            }
                            if (enemyMonster)
                                enemyMonster.GetComponent<Enemy>().PlayBumpSound(attackCounter);
                        }
                    }
                }
            }
        }
    }

    void RayInput()
    {
        if (Input.GetMouseButtonDown(1))
        {
            rightClickDown = true;
            curMark.SetActive(true);
        }
        if (Input.GetMouseButtonUp(1))
        {
            rightClickDown = false;
            curMark.SetActive(false);
        }

        if (rightClickDown)
        {
            RitualSymbol symbol = curMark.GetComponentInChildren<RitualSymbol>();
            Ray vRay = gameCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
            RaycastHit hit;
            if (Physics.Raycast(vRay.origin, vRay.direction, out hit))
            {
                if (hit.collider.tag != "Checkpoint")
                {
                    curMark.SetActive(true);
                    curMark.transform.position = hit.point + (hit.normal * 0.1f);
                    curMark.transform.LookAt(hit.point + hit.normal * 2);

                    if ((symbol.tagToInteract == hit.collider.gameObject.tag || symbol.tagToInteract == ""))
                    {
                        curMark.GetComponentInChildren<RitualSymbol>().MakeOpaque();

                        if (Input.GetMouseButtonDown(0))
                        {
                            if (chalk > 0)
                            {
                                audioExtra.clip = drawChalkAudio;
                                audioExtra.Play();

                                GameObject newMark = Instantiate(curMark);
                                newMark.GetComponentInChildren<RitualSymbol>().MakeOpaque();
                                newMark.transform.position = curMark.transform.position;
                                newMark.transform.rotation = curMark.transform.rotation;
                                newMark.transform.localScale = transform.localScale;
                                chalk--;

                                newMark.transform.parent = hit.collider.transform;
                                symbol.Activate(hit.collider.gameObject);

                                newMark.GetComponent<AudioSource>().Play();
                                hit.collider.gameObject.GetComponent<AudioSource>().Play();
                                if(hit.collider.gameObject.GetComponent<DisableTrap>())
                                {
                                    hit.collider.gameObject.GetComponent<DisableTrap>().Disable();
                                    audioExtra.clip = disableTrapAudio;
                                    audioExtra.Play();
                                }
                            }
                        }
                    }
                    else
                    {
                        symbol.MakeTransparent();
                    }
                }
                else
                {
                    curMark.SetActive(false);
                }
            }
            else
            {
                symbol.MakeTransparent();
            }
        }


        if (rightClickDown)
        {
            float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
            if (mouseWheel < 0.0f)
            {
                curMarkIndex++;
                if (curMarkIndex >= availableMarks.Count)
                    curMarkIndex = 0;
            }
            else if (mouseWheel > 0.0f)
            {
                curMarkIndex--;
                if (curMarkIndex < 0)
                    curMarkIndex = availableMarks.Count - 1;
            }
            curMark.SetActive(false);
            curMark = availableMarks[curMarkIndex];
            curMark.SetActive(true);
        }

        ActivateHoldingBoook();
    }

    void ActivateHoldingBoook()
    {
        if(curMark.activeSelf == false)
        {
            holdingBook.SetActive(false);
            //holdingChalk.SetActive(false);
            return;
        }

        if(curMarkIndex == 0 || curMarkIndex == 1)
        {
            //holdingChalk.SetActive(true);
            holdingBook.SetActive(false);
        }
        else
        {
            //holdingChalk.SetActive(false);
            holdingBook.SetActive(true);
        }
    }

    public float GetSpeed()
    {
        return speed;
    }

    public bool IsJumping()
    {
        return jumping;
    }

    public void PlayStepSound()
    {
        foreach (SoundRandomizer randomizer in stepAudio)
        {
            if (randomizer.groundTag == curTerrain)
            {
                int snd = Random.Range(0, randomizer.sounds.Length);
                audioStep.clip = randomizer.sounds[snd];
                audioStep.Play();
            }
        }
    }

    void PlayJumpStartSound()
    {
        foreach (SoundRandomizer randomizer in jumpStartAudio)
        {
            if (randomizer.groundTag == curTerrain)
            {
                int snd = Random.Range(0, randomizer.sounds.Length);
                audioStep.clip = randomizer.sounds[snd];
                audioStep.Play();
            }
        }
    }

    void PlayJumpLandSound()
    {
        foreach (SoundRandomizer randomizer in jumpLandAudio)
        {
            if (randomizer.groundTag == curTerrain)
            {
                int snd = Random.Range(0, randomizer.sounds.Length);
                audioStep.clip = randomizer.sounds[snd];
                audioStep.Play();
            }
        }
    }

    bool IsMarkAvailable(int id)
    {
        foreach (int b in book)
        {
            if (b == id) return true;
        }
        return false;
    }


}

