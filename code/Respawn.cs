﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Respawn : MonoBehaviour {

	public bool spawnCall=false;
	public float checkMin,checkSec;
	public List<GameObject> bookChk;
	public int countBooks=0;
	GameObject temp;
	// Use this for initialization
	void Start () {
		checkMin=0f; checkSec=0;

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void Spawn()
	{

		spawnCall = true;
		string bookMatch = "";
		//print (checkMin + ":" + checkSec);
		GameObject.Find ("Player").transform.position = transform.position;
		if(checkMin!=0 || checkSec!=0)
		{
		
			for(int i=countBooks;i<bookChk.Count;)
			{

				if(bookChk[i].ToString().Equals ("MarkA (UnityEngine.GameObject)")) 
				{
					bookMatch="Book1";
					//temp.transform.position = new Vector3(temp.transform.position.x,temp.transform.position.y+40,temp.transform.position.z);
				}

				if(bookChk[i].ToString().Equals ("MarkB (UnityEngine.GameObject)")) 
				{
					bookMatch="Book2";
					//temp.transform.position = new Vector3(temp.transform.position.x,temp.transform.position.y+40,temp.transform.position.z);
				}

				if(bookChk[i].ToString().Equals ("MarkC (UnityEngine.GameObject)")) 
				{
					bookMatch="Book3";
					//temp.transform.position = new Vector3(temp.transform.position.x,temp.transform.position.y+40,temp.transform.position.z);
				}

				if(bookChk[i].ToString().Equals ("MarkD (UnityEngine.GameObject)")) 
				{
					bookMatch="Book4";
					//temp.transform.position = new Vector3(temp.transform.position.x,temp.transform.position.y+40,temp.transform.position.z);
				}

				temp = GameObject.Find (bookMatch);
				temp.GetComponent<Renderer>().enabled=true;
				temp.transform.localRotation= Quaternion.identity;
				//temp.transform.position = new Vector3(temp.transform.position.x,temp.transform.position.y+40,temp.transform.position.)
				temp.GetComponent<Collider>().enabled=true;

				//GameObject.Find(bookChk[i].ToString()).SetActive(true);
				GameObject.Find ("Player").GetComponent<Player>().availableMarks.RemoveAt(i);
				GameObject.Find ("Player").GetComponent<Player>().curMarkIndex--; 
				bookChk.RemoveAt(i);


			}

			for(int i=0;i<bookChk.Count;++i)
			{
				print (i + ":" + bookChk[i].ToString ());
				print (i + ":" + GameObject.Find ("Player").GetComponent<Player>().availableMarks[i].ToString());
			}

			GameObject.Find ("UI").GetComponent<TextTimer>().mins = checkMin;
			GameObject.Find ("UI").GetComponent<TextTimer>().secs = checkSec;
		/*	for(int i=0;i<bookChk.Length;++i)
			{
				temp = bookChk[i];
				GameObject.Find("Player").GetComponent<Player>().book [i] = 0;
				print (bookChk[i]+",");
			}*/
			//GameObject.Find("Player").GetComponent<Player>().book = bookChk;
			GameObject.Find ("UI").GetComponent<TextTimer>().SetTime(new Vector2(checkMin,checkSec));

			//print (GameObject.Find ("UI").GetComponent<TextTimer>().mins + ":" + GameObject.Find ("UI").GetComponent<TextTimer>().secs );
		}
	}
}
