﻿using UnityEngine;
using System.Collections;

public class DisableTrap : MonoBehaviour {
    GameObject trapToDisable = null;
    public string disableTag = "Spike";

	// Use this for initialization
	void Start () {
        GameObject[] objs = GameObject.FindGameObjectsWithTag(disableTag);
        float dist = 0;

        foreach(GameObject obj in objs)
        {
            float distTemp = (obj.transform.position - transform.position).magnitude;
            if(dist == 0 || distTemp < dist)
            {
                trapToDisable = obj;
                dist = distTemp;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Disable()
    {
        if(disableTag == "FireTrap")
        {
            trapToDisable.GetComponentInParent<BoxCollider>().enabled = false;
        }
        trapToDisable.SetActive(false);


    }
}
