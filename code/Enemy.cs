﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    bool visible;
    public GameObject player;

    public GameObject dirLight;

    bool standing = true;
    public AudioClip attackSound;
    public AudioClip[] bumpSound;

    AudioSource audio1;
    AudioSource audio2;

    // Use this for initialization
    void Start () {
        AudioSource[] sources = gameObject.GetComponents<AudioSource>();
        audio1 = sources[0];
        audio2 = sources[1];
    }

    // Update is called once per frame
    void Update () {
        Vector3 position = player.transform.position;
        position.y = 0;

        Vector3 position2 = transform.position;
        position2.y = 0;

        Vector3 forward = position - position2;
        forward.Normalize();
        transform.rotation = Quaternion.LookRotation(forward, new Vector3(0, 1, 0));

        if(!standing)
        {
            transform.position += forward * 60.0f * Time.deltaTime;
        }
    }


    public bool IsStanding() { return standing; }
    public void SetStanding(bool st) {
        standing = st;
        if(!standing)
        {
            dirLight.SetActive(true);
            audio2.clip = attackSound;
            audio2.Play();
        }
    }

    public void PlayBumpSound(int count)
    {
        audio1.clip = bumpSound[count];
        audio1.Play();
    }

    void OnCollisionEnter(Collision C)
    {
        if(C.gameObject.tag == "Player")
        {
            if (!standing)
            {
                dirLight.SetActive(false);
                transform.position = new Vector3(0, -100, 0);
                Destroy(gameObject, 5.0f);
                player.GetComponent<Player>().SetEnemy(null);
            }
            else
            {
                Vector3 position = C.collider.transform.position - C.collider.transform.forward * 20;
                position.y = 0;
                transform.position = position;
            }
        }
    }
}
