using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour 
{
	public int yellow = 40;
	public int red = 30;
	public float updateInterval = 1.0f;  //How often we update the fps in seconds
	public float fps = 0.0f;             //Our final calculated fps
	private int frameCounter = 0;
	private float timeCounter = 0.0f;
	
	void Update()
	{
		if(timeCounter < updateInterval)
    	{
        	timeCounter += Time.deltaTime;
        	frameCounter++;
    	}
    	else
    	{
        	fps = (float)frameCounter/timeCounter;
			fps = Mathf.Round (fps);
        	frameCounter = 0;
        	timeCounter = 0.0f;
    	}
	}

	void OnGUI()
	{
		if(fps < yellow) 	  GUI.contentColor = Color.yellow;
		else if(fps < red) 	  GUI.contentColor = Color.red;
		else                  GUI.contentColor = Color.green;
		
		GUI.Label (new Rect (Screen.width - 50,0,100,50), "FPS " +fps); //(x,y,width,height)
	}
}