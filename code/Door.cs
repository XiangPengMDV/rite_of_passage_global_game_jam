﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public AudioClip loopSound;
    public AudioClip endSound;

	float x,y;
	bool anim=false;
	// Use this for initialization
	void Start () {	
		x=0.2f;
		y=transform.position.y + 0.1f;
	}

	public void OpenDoor()
	{
        if(!anim)
        {
            anim = true;
            GetComponent<AudioSource>().clip = loopSound;
            GetComponent<AudioSource>().loop = true;
            GetComponent<AudioSource>().Play();
        }

	}

	// Update is called once per frame
	void Update () {	
		if(anim)
        {
            transform.position = new Vector3(transform.position.x - x, transform.position.y - 0.5f, transform.position.z);
            x *= -1;

            if (transform.position.y < -y)
            {
                GetComponent<AudioSource>().clip = endSound;
                GetComponent<AudioSource>().loop = false;
                GetComponent<AudioSource>().Play();
                Destroy(gameObject, 5.0f);
                anim = false;
            }
        }
    }
}
