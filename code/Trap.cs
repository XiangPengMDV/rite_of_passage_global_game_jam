﻿using UnityEngine;
using System.Collections;

public class Trap : MonoBehaviour {

	bool danger = true;
	float dTime;

    public AudioClip playerDeathSound;

	// Use this for initialization
	void Start () {
		dTime = 0f;
	}
	
	// Update is called once per frame

	void OnCollisionEnter(Collision c)
	{
		if(danger)
		{
			if(c.gameObject.tag=="Player")
			{
				dTime = Time.timeSinceLevelLoad+2; //Time to display 'dead' message
			}
		}
	}

	void Dead()
	{
		GUIText text;
		text = GameObject.Find ("UI").GetComponent<GUIText>();
		if(dTime>Time.timeSinceLevelLoad)
		{
		GameObject.Find("Player").transform.position = new Vector3(transform.position.x,14,transform.position.z);
		text.enabled=true;
		text.text = "Dead!";
		text.transform.position = new Vector3(0.5f,0.5f,0f);
		}
		else if(dTime!=0)
		{GameObject.Find("Spawner").GetComponent<Respawn>().Spawn();
			text.enabled = false;
			dTime=0;
            GetComponent<AudioSource>().clip = playerDeathSound;
            GetComponent<AudioSource>().Play();
		}
	}

	void Update () 
	{
		Dead ();
		//If player clicks on it with RMB, disable: danger=false

	}
}
