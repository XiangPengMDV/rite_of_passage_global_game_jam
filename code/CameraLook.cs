﻿using UnityEngine;
using System.Collections;

public class CameraLook : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Ray vRay = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        Quaternion rot = new Quaternion();
        rot = transform.rotation;
        rot.SetLookRotation(vRay.direction, new Vector3(0, 1, 0));

        transform.rotation = Quaternion.Lerp(transform.rotation, rot, Time.deltaTime * 10);
    }
}
