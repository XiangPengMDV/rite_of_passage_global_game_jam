﻿using UnityEngine;
using System.Collections;

public class CameraBehavior : MonoBehaviour {

    float fixedLocalY = 0.0f;

    float walkLerp = 0.0f;
    bool walkLerpDown = true;
    
    public Player player;
    bool playerWalking;


    // Use this for initialization
    void Start () {
        fixedLocalY = transform.localPosition.y;
        playerWalking = false;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 localPosition = transform.localPosition;

        // If our position changed, which means we're walking
        if (playerWalking && !player.IsJumping())
        {
            if (walkLerpDown)
            {
                walkLerp -= Time.deltaTime * (player.GetSpeed() / 5);
                if (walkLerp < -0.5f)
                {
                    walkLerpDown = false;
                    player.PlayStepSound();
                }
            }
            else
            {
                walkLerp += Time.deltaTime * (player.GetSpeed() / 5);
                if (walkLerp > 0.5f)
                    walkLerpDown = true;
            }


            localPosition.y = fixedLocalY + walkLerp;
        }
        else {
            localPosition.y = fixedLocalY;
        }

        transform.localPosition = localPosition;
    }

    public void ResetLerp()
    {
        Vector3 localPosition = transform.localPosition;
        localPosition.y = fixedLocalY;
        transform.localPosition = localPosition;
        walkLerp = 0.0f;
        walkLerpDown = true;
    }

    public void StartWalking()
    {
        playerWalking = true;
    }

    public void StopWalking()
    {
        playerWalking = false;
    }
}
